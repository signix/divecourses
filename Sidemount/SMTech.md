# Sidemount Tech

### Tech vs Rec
 - Définition
 - Avantages
 - Risques

## Théorie
 - Calcul du SAC Rate
 - Rappel Nitrox
 - Hypercapnie

### Plannification
 - SAC Rate vs Volume Disponible (1/3) vs Temps souhaité
 - Gestion de la chaleur (chaud ou froid)
 - Logiciels de plannification (Multi déco, Subsurface)


### Travailler en équipe
 - Roles
 - Positionnement
 - Communication


## Techniques
### Flottabilité / Trim
 - Ajuster la flottabilité pendant la plongée pour travailler sur le milieu des poumons
 - Test du trim en s'approchant d'un fond (vide pour ne pas endommager le fond ou la vie marine)
 - Ne pas négliger l'impact des palmes (poinds, posiiton)

### Palmage
 - Grenouille (Frog kick)
 - Grenouille modifié
 - Battements (Flutter kick) modifiés
 - Hélicoptère
 - Reculer

### Stage/cylindres de décompression
 - 2 méthodes
  - Avec un bungee autour du coup de la bouteille (versatile, adapté pour une équipe sidemount)
  - Comme un stage classique (adapté pour une équipe mixte back/side)
- Demande un ajustement
- Marquage
- Penser à ajuster la flottaiblité lors de la pose/récupération


### Pratique Thetford
 - Test lestage
 - Palmage
 - S-Drill (Sidemount style)
 - Manipulation Stage
 - Calcul du SAC Rate
 - Plongée planifiée
 - Déploiement du SMB