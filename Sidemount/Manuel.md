# Sidemount

## Organisation du cours
 - Theéorie (en boutique)
 - Pratique en surface (Ajustement du harnais, échange de détendeurs, S-Drill, V-Drill)
 - Piscine
 - Milieu Naturel

## Objectifs
1. Sécurité
2. Progression

## Historique

## Le matériel
### Harnais
 - Séparé (plus versatile) vs intégré (plus facile à enfiler) vs hybride (plus mauvais). En H ou en V
 - Ajustement du harnais (ceinture sur les hanches, sangles de bras entre les omoplates), Placement des D-Rings épaule en face pouce quand bras levé à 90.
### Bouteilles
 * Alu vs acier 
    * [Faber](https://www.scubacenter.com/scubacenter_onlinestore/gas_management/Faber_Cylinders.htm)
    * [Catalina](https://www.catalinacylinders.com/markets/scuba/)
    * Tableau (https://greatdivers.com/scuba-gear/tank-size-chart/)
 * Cerclage vs sangle
 * Ajustement des bouteilles
 * Attache des bouteilles
   * Hautes
     * Loop vs continue
     * Ergo avec ou sans
     * Valve inversée avec ou sans
   * Basses
     * Position
     * Sliding D-Ring vs fixed
### Détendeurs
 - Idéal (tourelle + 5e port)
 - Alternatives
 - Drysuit (connexion à droite ou gauche)
### Accessoires et emplacement
 - Poche (dépend de la plongée)
 - Boltsnap
 - Spool + SMB (Spool réduit risques d'emmélement)

### Limites du Sidemount
 - Plongée profonde
 - Le mythe du bateau

## Les procédures
 * Enfiler le harnais [Vidéo de Gary Dallas](https://www.youtube.com/watch?v=I4J6uIOx0Cc&t=34s)
 * Montage des bouteilles (avant l'entrée, dans l'eau peu profonde, eau profonde, en flottabilité nulle) [Démo de Tomacz Michura](https://www.youtube.com/watch?v=VZSOJqZ6-os)
 * Gestion du gaz (échange tous les 400psi = sur long hose au début, milieu et à la fin 3000,2000,1000)
 * Panne d'air du binôme (partage sans laisser la bouteille, donner une bouteille)
 * Débit continu (Valve Drills, inflateur)
 * Perte d'1 ou 2 attaches hautes/basses
 * Utilisation de la poche
 * Enlever une bouteille
 * Déséquipement


### Entrées à l'eau possibles
 * Peu profond Solo
 * Plateforme, solo/team
 * Profond avec plateforme solo/team
 * Pas de géant/bascule côté ou autre
 * Échelle solo/team

## Hacking
* Harnais, inflateur de la bouée
* Poche
* Gadgets : Sliding D-Rings, Sump UK, bande élastique

## Sujets à discussion
### Lampes
 * Options : Godman Handle, cable, razor style
 * Canister

### Casque
* Utilisé pour : protéger la tête (point haut, pas de manifold), porter les lampes
* Nécessaire si DPV sous plafond (protection)
* Utile si solo
* Lampe génante en team
	
## HowToDive (pas seulement en side)

### Plan préplongé
 * Objectif principal de la plongée (secondaire par rapport à la sécurité)
 * Limites de la plongée
 * Entrées, sorties, route
 * Point d'intérêts, risques
 * Plannification gaz/décompression

### Compétences
 * La sainte trinité : Flottabilité/Trim/Propulsion (Position en bonus)
 * Palmage les types et leurs raisons

### Travailler en équipe
* Rôles
* Nos amis en double
* Responsabilités individuelles
* Communications

### Juste avant de partir
  * Vérification équipement (incluant solutions de secours : attaches, clips, masque, smb, suivant la plongée...)
  * Revue du plan
  * START
    * Safety Drill
    * Team check gear ( + Bubble check)
    * Air=Gas matching and usage
    * Route=entry,exit,path, objective
    * Table=dive limits (temps, profondeur, pression = Minimum Gaz, décompression)
    * [Article de TDI](https://www.tdisdi.com/tdi-diver-news/tech-dive-team-briefing/)
    * Mindset OK ?
* S-Drill, V-Drill
	

## Pratique piscine
 * Ajustement du harnais (si besoin) et des bouteilles
 * Trim
 * Alterner les détendeurs
 * Procédures dans l'ordre bouteilles, panne d'air, v-drill, déséquipement
 * Have fun in multiple orientation
 * Have fun removing the tanks
 * Enlever le masque (changer de detendeur)
 * S-Drill
 * V-Drill [Explications de Andy Davis](https://scubatechphilippines.com/scuba_blog/technical-diving-skills-valve-shut-down-drill-explained/)

## Sortie
* Pareil +  préparation des bouteilles (rangement long hose)

## Milieu Naturel 1
 * Entrée en eau peu profonde
 * Bouteille gauche puis droite
 * START
 * Palmage (frog) jusqu'à la plateforme
 * Récupération du détendeur
 * S-Drill
 * V-Drill
 * Récupération de la poche

## Milieu Naturel 2
 * Entrée en eau profonde sans cylindres
 * Récupération de la poche + sortie du masque
 * S-Drill
 * V-Drill

 ## Milieu naturel 3
 * Entrée en eau profonde avec cylindres
 * S-Drill modifié
 * V-Drill modifié
 * Pratique sur ligne
 * Lancement SMB (avec Spool)

## Références

[Montage XDeep](https://getaqui.com/stealth-2-0-sidemount-assembly-guide/)

[Ajustement](https://www.youtube.com/watch?v=Syjk94Ywb5o) ou  [ici](https://www.youtube.com/watch?v=eec-uDqmx-Q)

[Différents points de vue sur le Sidemount (EN)](https://gue.com/blog/the-whos-who-of-sidemount/)

[Histoire du sidemount](https://www.youtube.com/watch?v=ho8j24ujplE)