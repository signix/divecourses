## Boutique

### Trou
  - [ ] Tronconneuse ou scie manuelle + tarriere
  - [ ] Hachette
  - [ ] 2 Vis à glace
  - [ ] 6 Piquets
  - [ ] 2 rouleaux de tape rouge ou danger (solution écolo ?)
  - [ ] Harnais (selon le nombre d'étudiants)
  - [ ] Cordes (2 tailles différentes) + Dérouleurs

### Chauffage
  - [ ] 2 bouteilles de propane
  - [ ] Chaufferette
  - [ ] Bruleur + casserole "blé d'inde"

### Repas
  - [ ] Soupe chaude
  - [ ] Bols/couverts lavables
  - [ ] Sacs poubelles

## Participants
 - [ ] Pelle
 - [ ] Habits chauds + rechange
 - [ ] Thermos remplis d'eau chaude