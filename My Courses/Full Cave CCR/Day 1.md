Objective 1st days

Work as a unite



Schedule 8:30 to 6pm

1 day of lineplacement to join main lineplacement
1 day for team placmeent
No cave before day 5

As we are in CCR we will start navigation the sooner possible to avoid it to be too repetitive

Good CCR divers will get out with less O2 used, less deco and less CNS

Difficulty in cave diving is the diving part

Most of the problem is trying to do too many things at the same time


Accident slope

In cave you can anticipate what will happen

Awareness = Self => Team => Environment

Buoyancy => Trim => Position and only then action

Weight placement if unbalanced need more brain power

Everything we do is to make it easier ad have more brain availability

# Set Point Planification
 - [ ] Is Deco an issue
 - [ ] Is CNS an issue Time / Limit * 100
 - [ ] Is Oxygen consumption an issue (the more away we are from our dil PO2 the more we use O2) Think about the depth and profile of the cave
 - [ ] "Diveability" = Ease of dive


# CNS Table
| PO2 | Time |
|-----|------|
| 1.6 |  45  |
| 1.5 | 120  |
| 1.4 | 150  |
| 1.3 | 180  |
| 1.2 | 210  |
| 1.1 | 240  |
| 1.0 | 300  |
| 0.9 | 360  |
| 0.8 | 450  |
| 0.7 | 570  |


Checks
 1. Bubble
 2. Flow (left to right)
    - [ ] Left tank is ON
    - [ ] From my (side) tank I have a short/long hose, breath dry
    - [ ] From my (side) tank I have an inflator on my MAV/drysuit, MAV works/drysuit inflates and deflates
    - [ ] From my right tank my ADV is working, Cells are correct
    - [ ] Check O2 MAV + OPV
 3. Gear  (ARM => Shoulder => ARM => POCKET => BUTT => POCKET
 4. Limits (deco, SP, CNS %, Dive Time, BO range)
 5. Visualization