# Day 7

Tajma-Ha
|  | |
|-----------------|-------|
| Max Time        | 2.5h  |
| Avg depth       | 12m  |
| SP              | 0.8 |
| CNS%            | 150 / 450 => 33% |
| Deco            | ND |
| CO2             | Enough Absorbant |
| Bail Out Range | So much at least 45min |


When we see the slope. On the top of the slope are 2 arrows. There jump is on the left.
We should place the jump at the bottom of the slope.




## Complexe Navigation shortcut
Between all steps Swim into cave

 - WP1@<< (Swim to first double arrows, this will be Waypoint 1, if this was 3rd double arrows we add #3)
 - JL@WP1 (Jump Left at WayPoint 1)
- &#x21AA; WP1 (Return to WayPoint 1)
- &#x21AA; Exit ( Return to exit )


## Restrictions
### Minor restriction
Divers have to go single file, side by side not possible

### Major restriction
Backmount divers can not pass.

Two specific types (not done in cave class)
  - Tank off restriction, One tank has to come off
  - No mount restriction, both tanks has to come off

### What to consider
 1. Is there space on the other side ? (Can we turn after passing the restriction ?)
 2. Sediment ?
 3. Line placement
 4. Size of team (The smaller the cave, the smaller to team)

 Put the most inexperienced diver as diver 1. It will be easier for him to call the dive as they will have to figure the restrictions. On the way out, the most experienced diver being front, he can help.
 Out of Restriction the most experienced diver will make this a bit faster.


## Pass Restrictions
### On the way **In**
 1. Signal restriction
 2. Get "OK"
 3. Inspect restriction
   - Shape
   - Space
   - Line placement
   - Sediment
 4. Pass Slowly
 5. Back reference from the other side

### On the Way **out**
#### With visibility
1. Signal
2. Pass
3. Wait for team
#### Without Visibility





# Accident Analysis
Sheck Exley stated some rules for cave diving
 - Continuous Guideline
 - Gas Planning
 - Thre lights
 - Proper Training

 - Deep Cave Diving
 - Low Visibility
 - Panic
 - Overconfidence

 After some years there was still deaths and other causes was identified.
 - Solo Diving (still valid)
 - Deep Air Diving ()
 - Technological Advancement

The hazards were identified but they should be widespread and there was no regulation.

## Modern Era
Today most of the divers knows that overhead environment is dangerous (OW Chapter 1) and formal training is needed.
Today there is way less death and mainly due to overconfidence.

Other factors explaining the reduction of deaths are :
 - Significantly improved equipment
 - Accessibility or training
 - Increase in training quality
 - Self regulating communities (Mexico has a lot of regulation, Florida very few)

Accidents keep happening but WHO has accidents changed.
- Now majority are trained cave divers
- Some are unavoidable (Medical emergency, freak incidents like death of Parker Turner)
- Some are to be expected (Extrem Depth, Exploration)
- Other are avoidable because of
    - Overconfidence (I can manage it)
    - Outcome bias (Normalizaiton of deviance. It worked out last time, must be ok)
    - Social Conformation (Nobody else has a problem with it, must be ok)

**DON'T CONFUSE SURVIVAL WITH SUCCESS**

Incidents tend to involve one or more divergences from the divers standard procedures 
 - Analyzing / Labeling
 - Condition/Surface Checks
Usualyy divergencesare a result of pressure created by teammate or situation.
 - Time
 - Financial
 - Ego

### Community Responsability
As a community it is the responsability to all of us to contribute to a safer diving culture.
We can not have 0 incident but we should continuously work to minimize them by : 
 - Prevention
 - Analysis
 - Implementation




Book to read : Caverns Measurless To Man
To Watch : A cave diver story





Might go to fake Mayan Blue on the last day