# Day 10

| Plannification                |                 |
|-------------------------------|-----------------|
| SP 1 Everything before 2nd T  |             1.0 |
| SP 2  Everything after 2nd T  |             1.3 |
| CNS 1 duration                |        64 (22%) |
| CNS 2 Duration                |        88 (49%) |
| Total CNS                     |            71 % |

Max deco (based on O2 available) 25min
Estimated deco 23min@1.3

 

Navigation
 - TR
 - TL
 - WP1@>
 - JL@WP1
 - Sloath bones
 - &#x21AA; WP1
 - TR
 - EOL
 - &#x21AA; WP1
 - Exit



 Ascent together because you never know (hypoxia happens sometimes on CCR while surfacing)

 # Psychological Aspect

 ## Motivation
 Discovery, passion are positive
 Ego, overconfidence, pressure (peers, money...) elevate the likelyhood of accidents (lack of concentration and focus)

 ## Stress
If untreated will result in pANIC which is a almost certainly a death sentance
**Early recongnition**  is key to prevent PANIC. Calm down before taking decision.
Often easier to see on others than on ouself. Stressed divers often lack awareness.

## Psychological stress factos
- Team/Instructor
- Gas reserves
- Orientation
- Distance
- Decompression
- Restrictions
- Limited visibility
- ...

## Physiological stress factors
- Cold/Hot
- Hungry / Thirsty
- Toilet
- Flow / Fatigue / Cramp
- Equipment
- Equalizing, Reverse block
- ...


STOP, BREATH, THINK, RELAX AND ONLY AFTER Act
Listen to your inner voice. If you think about calling the dive, the dive is called for you, just inform the rest of the team.

**Attitude** is very important: Believe you can, believe you can't. Either way you are right.

