# Day 4

Markers
120 * 11.1 / 23v5 /2.27


Do  BO Range Table up to 24m and 150b

Navigation is the main factor of death





## Markers types

Directional markers
Dorf Markers : directional was duck tape folded in half to create a form of arrow
Arrow : directional Crated on a cave conference


Cookie : non directional created in Mexico by by Dany Reordal, always placed on my exit line


REM : directional created by Bill in Mexico in early 2010 ish. Reference Exit Marker. Same role as arrow
      Avoid people being confused with added lines by another team
	  Not known by everybody
	  
Cloth pins : used by french and Swiss people, non directional, fast but break or falls off

## System Markers
### Single arrow 
- In Florida mark distance to exit
- In Mexico Mark jumps. no distance because there are many entrances and to avoid ego dive to reach the x meters arrow. 

### Double arrows
- In Florida : Mark Jumps
- In Mexico : Mark Circuits or Traverse

### Reverse arrows
"Halfway" point. Very approximative. Might have more depth or restriction  on one side. Might not be halfway to exit (could need jumps).

### Reverse arrows + another one
Prefeered exit. Indicate the best exit.

### Cancelling an arrow
By adding a cookie on an arrow facing the wrong direction for us. We can cancel it.
Some team cancel all the arrows pointing in the wrong direction. Other teams just cancel the first arrow to bookmark the fact that from now on we can ignore arrow direction. Others never cancel arrow.


## Navigations

### Ts
 - Temporaray :  Created temporary for a join line or a jump line
 - Permanent : 
   - Fixed T: There is a Tie-off at the intersection, arrow placed a bit before the tie off
   - Floating T : No Tie-off, just a line in the mildle of another one
   - Hidden T: 
   
  Marked by placing a cookie on the exit line

  On the way in
  1. Signal (big signal no cover)
  2. Show Cookie
  3. Place Cookie
  4. OK From Team
  5. Mental image

  On the way out
  1. Signal
  2. Confirm exit
  3. Remove cookie
  
### Jumps
 - Marked jump are signaled with an arrow on the main line
 - Unmarked jumps are not marked on the main line
 - Hidden jumps are not marked on the main line and not visible from the main line
 - Reach Jumps are within arm reach or on the same attachment point of the main line (not used anymore in Mexico, 3m minimum between a jump and the main line)

Direction jump names
 - Middle to end (or Middle to start). Are jump from the middle of the line to end of antoher line
 - End to Middle From the end to the middle
 - Middle to middle From the middle of a line to a middle of another another line
 - Gaps (end to end) from the end of one line to the end of another. Less common today. Could be used to separate line (to be used by Intro to cave divers who can not do navigation).



#### Gentleman rule
You should never add new navigation decision for people already in the cave
Team 1 connect further from the end of line and others always connect closer to the end of line


How to do Jump
1. Signal jump (Omega sign)
2. If needed, place marker (Arrow)
3. Spool out and place it (add tension, backreference...)
4. If needed place cookie
5. Mental image



Tomorrow Ponderosa