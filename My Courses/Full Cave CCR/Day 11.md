# Day 11

Nohoch Nah Chich


## Planification
|        ||
|----------------------|----------|
| Average Depth        |       5m |
| SP                   | Dil (.8) |
| CNS                  | tones    |
| Deco                 | 0        |
| BO Range             |   116min |
| Sorb                 |    4h OK |


## Navigation
- JL @ <
- JL @WP1 Hidden Left in Ocean of Stalagtites 
- TR in Restriction 2
- &#x21AA; WP1
------------
- JR@<
- JL@EOL
- JR@<
- JL@2m after <1
- JR@<
- &#x21AA; Exit

Weight (fresh)
0-2-0-2 (4lbs)

# Cave formation

## History and Terminolgy
A cave is a natural opening in the ground, beyond dailight zone, large enough for humans to enter.
Phreatic zone is the part of an aquifer, below water table, staurated with water.
The Vadose zone (unsaturated) is the part of the earth between the land surface and the top of the phreatic zone.

Caves form in both Vadose and Phreatic zones

## Dissolution Caves
Most complex systems (Mexico, Florida, Bahamas, Spain, France...)

Formed by dissolution of soluble rock (limestome, but also dolomite and gypsum)

Rain abdobd CO2 from soil and atmosphere to create acid (CO2 + H20 = H2CO3 carbonic acid). Acid then dissolve the limestone

Tectonic activity laso shapes solution caves.

Halocline will retain carbonic acid, leading the cave to be larger at the halocline


### Bedding plane 
 Officialy a surface separating 2 different layers of stratified rock.
 Within cave diving, it is used to describe a low wide section of cave

 ### Canyons
 Vertical cave, could be quite narrow and deep.

 Good reason for people to dive without ADV (plugged).

## Cave formation

Check [Maria's article on Protech website.](https://protecdivecenters.com/blog/cave-formations/)

Speleotherm forms when the cave is dry by calcite left by water.

Stalactite are sharps when Stalagmites are more rounded.

Whene they join they form a column.


## Other Types of Caves

### Glacier/Ice caves
#### Glacier
- Very unstable
- Very rare
- Possibly ery strong flow
Don't do it

Ice cave are caves of any type containing cave made of ice

### Ocean/Sea caves

Formed by wave action
All hazards from ocean and from overhead

- Might have sea life (poisonous, shark...), Sharp rocks

- Dangerous currents/flow due to tides

- May be hard to exit

### Lava cavesFormed near volcanic area by surface cooling of lava flows

In places like Hawaii where lava tubes are connected to the ocean, they'l have same hazards as sea caves

Very dark and can have sharp edges (might require drill to create attachment points)
Usualy not much navigation but darkness absorb light

### Coral/reef caves
Formed by gorwing reef

Swim through (no line !) In Utilla one is a more than 1h Swim
Usually limited in length and size

All hazards from ocean (Currents, surge, surf, marine life, might have sharp edges)

### Tectonic caves
Formed by later movement of the bedrock known as "gravity sliding"

High narrow fissures

Shifting may occur (on a geological scale pretty unliquely on human life scale)

### Mines

principals of diving are the same
Mines are manmade and inherently a higher rick environment than the vast majority of natural caves:
 - Stability
 - Rick of entanglement
 - Sharp objects/machinery/tools
 - Water/Air tocicity
 - Usually easier to navigate as we can find maps and logic

 # Karst Environment

 Karst topography is a landscape formed by the dissolution of soluble rocks.


 A Karst window is a special type of sinkhole that gives us a view, or window, into the kast aquifer.

 Might have different names:
  - Mexico = Dznot/Cenote
  Dominican Republic = Manantial
  Cuba = Casimba
  Bahamas: Blue Hole (might not be exactly a karst window)
  Florida = Spring/Sinks
  France = Résurgence


### Sinkhole
  A sinkhole is a depression or hole in the ground caused by some form of collapse of the surface layer.

  Sinkhole vary from shallow holes about 1m deep to pit more than 50m deep
  Pond like sinkholes are usually not connected to horizontal cave.

### Sump
Sump is a passage in dry cave that is submerged

Static: No inward or outward flow
Active : continuous through-flow (could be seasonal)

When short in length could be called a duck

Mixing of dry caving and cave diving

### Anchialine Pools

Landlocked body of water with subterranean connection to Ocean
Specific ecosytem
Water near the surface being fresh, salt water below

### Conservation
Cave diving is a privilege to see things that have been preserved for hundreds or thousands of years.
Minimal impact on the cave
- Bouyancy control
- Move with caution

Correct behaviour includes : 
- Stay close to the line
- Never touch/remove anything
- Be considerate of life forms

Follow local regulation

Maintaining landlowner relations is done as a community, be a positive impact

Avoid touching speleotherm but sediment too (don't leave fingerprint all over the place, don't silt outward).

If we have to tuch the bottom try to chose solid rock. Never push the end of a speleothrm (prefer the base if unevitable).

The more fragile the environment, the better diver we should be.



To Watch  Nova the first face of america