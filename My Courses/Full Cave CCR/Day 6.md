# Day 6
Lost line and lost diver happen because of complacency and broken rules.


## Lost diver

NEVER PUT YOURSELF AT RISK

1. Turn around and look, with and without light.
2. Place an Arrow on the line
3. On OC Check gaz range if you are not exiting.
On CC Calculate Time To Exit based on Bailout Range.
4. Search
    1. Look with and without light
    2. Leave Markers behind on the line (keep at least 1 arrow) will help divers find the line
    3. Leave Backup Light (because cave line might not be seen from far away)
5. If found
    1. Swim to the closest place on the line
    2. Place an arrow
    3. Try to Signal
    4. Safety spool out
    5. Place line around them so they can find it (not straight to them).
    6. On a team of 3 the 3d diver stay on the line at the connexion point, unless he loose communication with the searching diver (then he should moove on the spool to keep communication)
    
6. If not found ... go home and call for help

## Lost line

### Lost line **with** visibility
Either you passed the line or you choose to leave the line (what are you doing ?)

1. STOP (not getting more lost)
2. Look with and without light
3. If you don't see light, Do a STAR SEARCH
   1. Make Tie-Off with your safety spool
   2. Search one direction, go back try another one...
   3. When line found, attach spool + Exit


### Lost Line **without** visibility
1. Fan out (arms and legs extended) and descend 
2. Deflate everything
3. Relax and think (What is my PPO2 and how many time do I have ? Where am I ? Where is the line ? Is it on the floor ? On the ceiling ? Where should I search ? Are there slope or halocline that could help for reference). Do not make it worse !
4. Find **2** Tie-Offs. The stronger one should be the second one
5. If the very unlikely case you are in a very big room. Ascend to exit silt.

5. Grab spool and do Star Seach from Secondary Tie-Off
    Search straight and far enough
    If visibility stays bad you are probably in the good direction :D





#
The open size of the Tie-Off will influence what is easy or not and how easy it will fall when coming back on touch contact.



## Line types
95% of cave in Mexico is coverd with #18 twisted line.
Because this is what you can find in the fishing store
It is the thinest you can find (so you can fit more in your reel).
It is weaker
If you explore with it. Expect it to break.

### Twisted Line
Twisted line is just 3 lines twisted together. In theory is more resistant to tension but less resistant to abrasion.
It is used for something that will be fix and not moved.


### Braided line
Usually #24 is more resistant to abrasion and is used on spool and primary reel as they are placed and remove multiple times.
Can be used for everything.
TO have a spool with some, order to DGX and asked for #24 line. They will replace the line.
In Europe ask TecMe or DeepStop in Germany.

### Kermantle line
Braided outer shell, Twisted line or bungee in the core.
Used on cavern line


## Line material
### Nylon
The best optoin : stretches and sinks.

### Polypropylène
Can not strectch not possible to create tension. Create loose line.


## Line length
- Jump Spool : At least 30m
- Safety Spool : 20-30m (should not be further than that of the line)
- Primary Reel : 400ft/120m




# The life of a cave (Exploration)
Fist day on wetsuit place a prmiary Tie Off on land to get a GPS point (named ZD = Zero Datum).
In the software you need a starting point. Could be an existing line in the software or an ZD for referece.

Then you lay line in the cave.

At the end
 - With survey slate.
Take slate an write TAL (Time, Azimuth Length) for each section. The last one should be 0D

Everytime you see a tunnel. You place arrow to indicate leads.
Second dive you go in and decide where you want to go.
you tie off on the tie off efore the arrow

Now instead of Exploration Slate we use MNemo.
