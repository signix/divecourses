# Day 8

Planificaiton
Minautoro


| Minautoro | |
|-----------------|------------------|
| Max Time        | 2.5h             |
| Avg depth       | 14m              |
| SP              | 0.8              |
| CNS%            | 150 / 450 => 33% |
| Deco            | ND               |
| CO2             | Enough Absorbant |
| Bail Out Range  | 70 min           |



## Hazards of cave diving

We don't have much injury (except bends). Either people comes out or not.

### Visibility
Usually reduced by sediment moved in the water by divers or environmental conditions
#### Thicker materials
Sand
Mung : found Downstream (organic material), usualy big anough
Calcite Raft

#### Thiner materials
Mud or silt take slower time to settle
Clay takes even more time to being very fine but require contact to stir up due to electromagnetic properties

Other cause of reduced visibility
 - Tannic Acid
 - Hydrogen Sulfide
 - Algae Bloom
 - Halocline

### Flow
#### High Flow
 - Only upstream dives
 - Might be seasonal
 - No long penetration without additional gear
 - Can break/damage line
 - Less sediment, better vis
 - Easier and less navigation
 - Constant swimming required to progress
 - Rule of thrid works well

#### Low - No Flow
- Require stronger basic skills
- Lots of fine sediments
- Low visibility more likely
- Rule of thirds as bare minimum
- Very complex cave systems
- 365 days a year
- Broken guide line drops to floor
- Big dives with little gear

#### Line traps
Physically not able to follow guideline in touch contact
Can be horizontal AND vertical
Very dangerous in 0 viz

