
# Nitrox Avancé

## Hyperoxie
Principal risque ajouté par l'utilisation d'un málange suroxygéné pendant la plongée.
Réaction des poumons (pauses à l'air)

## Choix du mélange
 - Calcul de la profondeur équivalente à l'air 
 - Narcose
 - Décompression
 - Densité idéalement < 5.2g/l et toujours < 6.2 g/l = 38m  voir recheche du Dr. Simon Mitchell and Gavin Anthony https://dan.org/alert-diver/article/performance-under-pressure/
 Problèmes possibles : rétention de CO2, Travail respiratoire (risque d'OPI), augmentation du CO2 (hypercapnie, narcose)

### Pratique
 - Calculer la profondeur maximal à l'air ?
 - Calculer la profondeur maximal avec un Nx 36 ?
 - Mise en situation, plongée sur le Eastcliffe Hall (21m) quel est le meilleur mélange ?
 - Plannifier 1h max de plongée sur ce site avec de l'air ? avec le mix idéal ? avec une déco O2 ? Quel volume sera pris ?
 - Vérification O2 clean bouteille
 - Marquage des cylindres, des 2e étages

## Manipulation de l'O2
 - Révision des méthodes de remplissage (Pressions partielles, Stick, Premix, membrane,  mass fraction)
 - Risque de manipulation, compression adiabatique, ouverture des cylindre
 - Révision analyse, marquage 

## Changement de gas
 - Au palier
 - En transition
https://www.tdisdi.com/tdi-diver-news/how-to-switch-your-diving-gas/
 - Configuration de l'ordinateur

https://www.youtube.com/watch?v=EzseZckp0MQ


Change To Different Tank
Check (Label with name and gaz, depth)
Turn Valve On
Déployer 
Team confirmation

### Acronymes
 - [MODS](https://www.tdisdi.com/tdi-diver-news/gas-switch-roulette/) chez TDI (Mix vérification, Ouvrir, Depth = profondeur vs MOD, Switch confirmation), [Version détaillée](https://www.tdisdi.com/tdi-diver-news/how-to-switch-your-diving-gas/). Fait partie des [3 protocoles de TDI](https://www.tdisdi.com/tdi-diver-news/three-safety-protocols-for-technical-diving/)
 - [SWITCH](https://www.divessi.com/en/blog/decompression-gas-switch-7142.html) chez SSI (Signal, Water depth, Identify, Turn, Confirm, Hang)
 - [NOTOX](https://daivings.lv/en/notox-gas-switching/) chez PADI (Note cylinder, Observe depth, Turn on valve, Orient 2nd stage, eXamine teammate)

### Pratique sur chaise avec bouteille
 - Changement de gaz en équipe






# Standard du cours
 - [Procédures de décompression](https://www.tdisdi.com/tdi/get-certified/Advanced-Nitrox-Diver/)