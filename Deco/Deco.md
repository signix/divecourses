# Décompression
## Théorie et histoire de la décompression
 - Grandes dates
   - 1908 Haldane : 5 compartiments (5,10,20,40 et 75min de demi-vie). Différence de pression critique de 2:1 puis 1.58:1
   - 1960 Workman : Différence en pression absolue spécifique à chaque compartiment, ajout de 3 compartiments : 160, 200, 240
   - M-Value : Différence de pression acceptable pour éviter un accident de décompression
   - Bühlmann : Plusieurs variations dont ZHL16C
 - Paliers profonds (remis en cause par plusieurs études https://pubmed.ncbi.nlm.nih.gov/15892549/)
 - Choix des GFs. Inférieur à 80 : https://www.youtube.com/watch?v=DjOx1kcIrTc (données de [DAN Europe](https://www.researchgate.net/publication/319914895_Dive_Risk_Factors_Gas_Bubble_Formation_and_Decompression_Illness_in_Recreational_SCUBA_Diving_Analysis_of_DAN_Europe_DSL_Data_Base))
## Stratégies de décompression
Les stratégies de décompression cherchent à réduire le risque mais ne l'élimine pas. Elles ne sont que des modèles mathématiques qui n'ont aucune connaissance de votre corps.

### Modèle à bulle : RGBM (Suunto, Mares)

### Modèle gazeux : Buhlman ZHL-16C avec GF (Shearwater, Garmin)
Logique : Chaque compartiment "gaze" et dégaze selon sa demi-vie

## Facteurs impactant le risque d'ADD

###

## CO2
[DAN CO2 part 1] (https://alertdiver.eu/en_US/articles/carbon-dioxide-the-dreaded-enemy-part-1/), [DAN CO2 part 2] (https://alertdiver.eu/en_US/articles/carbon-dioxide-the-dreaded-enemy-part-2), [DAN CO2 part 3] (https://alertdiver.eu/en_US/articles/carbon-dioxide-the-dreaded-enemy-part-3)

### Cycle menstruel
Pas de preuve mais peut être plus de risque pendant les menstruations. Cela pourrait s'expliquer par une vasoconstriction (plus de sensibilité au froid) mais aucune preuve pour l'instant
https://dan.org/alert-diver/article/womens-health-in-diving/
https://www.dansa.org/blog/2016/06/14/menstruation-during-diving-activities

# Gestion du temps et des imprévus


## Accélérer sa décompression avec l'Oxygène
 - Avantages
 - Risques (voir Nitrox), changement de gaz

## Incidents
 - Omission de palier
 - Plongeur inconscient


## Plannification
 - Avec des tables
 - Avec un logiciel (Subsurface ou Multi déco)
 - Ratio déco
 - Calcul des gaz d'équipe
 - Configuration de l'ordinateur

### Pratique
Durée max d'une plongée de 1h max avec décompression sur le Kinghorn (27m/90ft) à l'air ? avec un Nitrox adéquat ? avec une déco O2 sans Nitrox ?
Combien de temps possible sur le Gaskin (0-18m/60ft en 25m pour le trajet aller) ?
Plannifier une plonger de décompression aux escoumins (fond à 300m/1000ft)


 
### Autres points
[If only](https://www.thehumandiver.com/ifonly) Reportage sur le comportement humain et la normalisation de la déviance

https://gue.com/blog/what-is-undeserved-in-undeserved-decompression-sickness/
[Ratio Deco utilisé par certaines agences](https://www.youtube.com/watch?v=jJ7_JFjzZ60)


## Matériel
### Détendeurs
 - Double
 - Sidemount

### Lampes
Rangement
https://www.youtube.com/watch?v=Xgay6xG-vhs

### Pratique
 - START
 - Partage d'air
 - V-Drill
 - Lancement de parachute


 # Milieu Naturel
 ## Jour 1 côté peu profond
  - START
  - Dépose, récupération du cylindre de déco
  - S-Drill
  - V-Drill
  - Déploiement du lift bag/smb
  - Freeflow sur BCD
  - Remorquage plongeur sur 30m profondeur et surface
  - Promenade + calcul SAC
  - Palier + calcul SAC deco

## Jour 2
  - Apnée sur 15m
  - Manipulation Ordinateur
  - Ordinateur 40/40

## Jour 3
 - Buddy breathing deco gaz
 - Résolution de problème (décompression oubliée, temps de fond + 5min, perte d'un gaz de décompression)


# Standard du cours
 - [Procédures de décompression](https://www.tdisdi.com/tdi/get-certified/decompression-procedures-diver/)