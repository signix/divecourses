# Planification
 - Définir l'objectif
 - Définir le lieu
 - Définir les exigences (compétences, équipements,  personnes additionnelles)

### Réunir l'équipe

 - Définir les limites (Profondeur, Durée, Déco, Air)
 - Identifier les risques
 - Identifier les aspects spécifique du site : mise(s) à l'eau, sortie(s0), plan d'urgence
 - Définir la route
 - Plannifier la décompression : Choix des gaz, profondeurs des palier, algorythme
 - Plans alternatifs : durée plus longue, profondeur dépassée, perte de déco

