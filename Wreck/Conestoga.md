# Lock 27-28: Du Wee-Hawk au Conestoga

## Histoire
Le Wee Hawk était une barge de construction utilisée dans les années 50. Elle finit par être amarrée puis démantelée.
Suite à la construction de la voie maritime(inaugurée en 1959) les canaux furent abandoné. Le canal 28 fut remblayé en gardant seulement une petite baie dans laquelle on retrouve le Wee Hawk et le lock 27 n'est plus vraiment utilisé autrement que pour de la plaisance ou la baignade.
Le Conestoga était un bateau à vapeur de 253ft de long lancé en 1878. En 1922, alors qu'il attendait pour passer l'écluse 28, un feu éclata dans la salle des machines. Après avoir été dégagé de l'écluse, le bateau coula à sa position actuelle.

## Plongée
ATTENTION Le site est partagé avec d'autres usagers. Il a failli être interdit aux plongeurs au printemps 2023. Il convient donc d'être particuliérement attentif aux autres usagers et à notre comportement.

### Profondeur
Profondeur: max 10m/30ft, moyenne de 12-15ft.

### Visibilité
La visibilité est affecté par les conditions météo (pluie, vent) et par la saison (15-30ft en été, 45ft et + en été)

### Résumé de la route
La plongée regroupe 2 épaves, le Wee Hawk et le Conestoga avec une dérive dans l'écluse 27 puis le St Laurent entre les deux. Il est possible de faire une légère pénétration dans la Wee Hawk.

### Température
La température de l'eau est souvent assez élevée (jusqu'à 23C en été) grace aux eaux de surface du lac Ontario qui sont aspirées par le St Laurent.
Attention aux coups de chaleur (ou de froid en hiver) en attendant les voitures en cas de dérive.

### Faune
Le sol est couvert de moules zébrés et de gobie à taches noires qui sont deux espèces envahissante mais on y croise aussi beaucoup d'espèces indigènes (doré, perchaude, borchet, ecrevisses)

### Note
En tout temps il est possible de rejoindre la rive en cas de problème, celle-ci étant toujours proche.


## Orientation
<figure><img src="https://scubaboard.com/community/attachments/weehawktoconnie-gif.380007/"/><figcaption align="center">Trajet (source Scubaboard)</figcaption></figure>
Le départ de la plongée sur le Wee Hawk se fait depuis le Wee Hawk qui est accessibl au bout de la route  [Canal Galop à Cardinal](https://www.google.com/maps/place/Wreck+Wee+Hawk/@44.7769353,-75.4001185,17z/data=!3m1!4b1!4m6!3m5!1s0x4ccdad1cf75516d7:0x26d91559c404acb3!8m2!3d44.7769353!4d-75.4001185!16s%2Fg%2F11cnq4dm3s?hl=en). Un stationement permet de laisser les voitures.
En cas de dérive vers le Conestoga, il est recommandé de laisser une autre voiture vers le [Conestoga](https://www.google.com/maps/place/Conestoga/@44.7795598,-75.3955519,17z/data=!3m1!4b1!4m6!3m5!1s0x4ccdad190fd6403f:0x3b1a55306a6c0c56!8m2!3d44.779556!4d-75.392977!16s%2Fg%2F11c3q595kh?hl=en).

### Mise à l'eau
L'ensemble du matériel peut être apporté au bord de l'eau à pied et on peut s'équiper à la dernière minute. La mise à l'eau est abritée du courant il faut ensuite remonter en direction du Canal pour s'approcher du Wee Hawk.Habituellement le tour se fait en partant en direction Est (vers la porte) puis Sud pour ensuite avancer vers le centre de l'écluse ou le courant va s'intensifier.
### Dérive dans le lock 27
On se laisse ensuite porter par le courant.
Quand le canal s'élargit, le courant va réduire, après quelques minutes il faut se rapprocher du bord Sud/Sud-Est pour traverser et passer dans le St Laurent. En raison de la faible profondeur au passage vers le St Laurent, le courant y est plus fort mais ralentira ensuite. Rien ne sert de nager contre le courant. Il faut nager en direction du Nord/Nord-Est pour partir en direction du Conestoga.
### Le St Laurant
Une fois dans le St Laurent, attention à ne pas trop se rapporcher de la rive sous peine de subir le contre courant et de devoir palmer fortement pour rejoindre le Conestoga. Dans ce cas il peut être préférable de sortir pour marcher jusqu'au Conestoga.
Si tout va bien vous palmerez jusqu'à atteindre le Conestoga. 
### Le Conestoga
![Map Conestoga](https://saveontarioshipwrecks.ca/wp-content/uploads/2018/03/dguide3a.jpg "Conestoga")

La proue du Conestoga devrait apparaitre devant vous. Vous pouvez vous réfugier à l'intériuer de l'épave ou se trouve le plus de choses à voir et une relative protection du courant.

Après avoir parcouru le Conestog vous descendrez à la poupe ou on peut observer l'hélice et, si vous avez assez d'air, remonter le long d'une chaine (entre la rive et l'épave) pour faire une deuxième visite.
### Sortie
La sortie se fait habituellement en aval du Conestoga, proche du stationnement des voitures.


## Risques
### Courant
Ce site est exposé au courant du St Laurent. Il ne sert à rien de lutter contre lui mais planifier votre route et la procédure à adopter en cas de pertede binôme.
### Pénétration
La pénétration dans le Wee Hawk doit se faire dans les limites de vos compétences. En particulier la deuxième salle peut rapidement perdre sa visibilité.
### Navigation
Il y a peu de navigation de bateau mais celle-ci reste possible donc attention si vous faites surface loin de la rive. En particulier dans la canal tenez-vous près des bord si vous faites surface.
### Blessures
Le Conestoga est un bateau en métal et il est donc possible de s'y couper ou de se cogner sur certaines parties. Le plus simple pour l'éviter est de regarder la direction dans laquelle on se déplace (courant)

## Points d'intérêts
### Wee Hawk
L'intérieur du WeeHawk est visible depuis une ouverture située sur la face Nord-Est (la première en arrivant dessus) sur la gauche. Il contient une chaudière. Plusieurs poissons (dorées, perchaudes, achigan) profitent de la protection du bateau et sont visibles sous la coque.
### Dérive
La dérive dans l'écluse (et relativement dans le St laurent) permet d'avancer sans se fatiguer et de croiser plusieurs poissons (dont des brochets dans les ahutes herbes).
### Conestoga
Le Conestoga de par sa faible profondeur permet d'avoir un temps de fond conséquent. Plusieurs engrenages sont encore visible ainsi que les chaudières, les chaines.


## Informations complémentaires
 - [Article de Save Ontario Shipwreck](https://saveontarioshipwrecks.ca/wp-content/uploads/2018/03/TheWreckOfTheWeehawk.pdf)
  - [Scubapedia Wee  Hawk](https://mail.scubapedia.ca/index.php/Wee_Hawk)
  - [Conestoga](https://mail.scubapedia.ca/index.php/Conestoga)
  - [Histoires des canaux et photos d'époques](http://stlawrencepiks.com/seawayhistory/beforeseaway/galop/)
  - [Article Wikipedia sur la voie maritime](https://en.wikipedia.org/wiki/St._Lawrence_Seaway)