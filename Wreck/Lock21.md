# Lock 21

## Histoire
L'ecluse fut construite de 1834 à 1843. Elle faisait parti d'un ensemble de 6 écluses permettant de traversers les rapides de Long Sault.
Suite à la construction du barrage de Cornwall et aux modificaitons de la voie maritime, l'écluse 21 (et plusieurs autres) fut submergée.

## Plongée
La plongée se fait depuis le park de Long Sault à côté de l'ile Macdonell Island (frais à payer à l'entrée du parc). C'est une plongée dans un fort courant qui permet d'observer l'écluse.
Le site est référencé sur [Google Map](https://www.google.com/maps/place/Lock+21,+Long+Sault+Pkwy/@44.9977026,-74.8993807,337m/) donc assez facile à trouver. Le départ se fait proche des tables à pic-nic dans un parc très agréable.

## Orientation
<figure><img src="https://saveontarioshipwrecks.ca/wp-content/uploads/2018/03/dguide2a.jpg"/><figcaption align="center">Carte Écluse 21 (Save Ontario Shipwrecks) </figcaption></figure>
La plongée se fait le long de la structure en passant d'abord sur les portes de l'écluse puis en dérivant entre la partie en aval des portes et dans le reste de l'écluse. A la fin de la structure on peut se diriger vers le nord pour sortir sur la berge.


### Mise à l'eau
La mise à l'eau se fait depuis l'île MacDonnell. Une bouée et une ligne de surface permettent de s'avancer dans la partie peu creuse  dans une petite baie relativement protégée du courant. En suivant la corde qui part de la bouée vers les USA vous descendrez sur le haut de la structure (40ft) que vous pourrez suivre. Profitez en pour descendre voir les portes depuis l'amont. Certaines sont ouvertes et permette de les traverser pour profiter de la protection de l'écluse. Le courant dans les ouvertures de l'écluse peut être assez fort mais ralenti dès qu'on a traversé.

### Dérive dans le lock 21
Une fois la structure des portes passée il est possible de se laisser dériver dans l'écluse ou en arrière des portes. Il est recommandé de se garder une réserve d'air afin de traverser l'écluse pour sortir.

### Sortie
Après votre dérive au bout de l'écluse une corde vous emmenera vers le Nord-Nord/Ouest en direction du rivage. Suivez là pour ressortir dans le parc non loin des tables à pic-nic.

## Risques
### Courant
Ce site est exposé au fort courant du St Laurent. Il ne sert à rien de lutter contre lui mais planifier votre route et la procédure à adopter en cas de perte de binôme peut simplifier votre plongée. En cas de doute prendre plein Nord vous permettra de sortir.

### Visibilité
La lumière est assez faible sur ce site et une lampe vous permettra de mieux profiter de votre expérience. De plus avec le courant il y a souvent beaucoup de particules.

## Points d'intérêts
Les portes sont un point majeur pour mieux apréhender à quoi pouvait ressembler la voie maritime il y a 150 ans. En traversant on peut également observer les fondations (surtout une dalle de béton avec quelques marques) de l'éclusier qui fait partie des bâtiments démolis.
Plusieurs poissons se cachent derrières les portes ou jouent dans le courant et peuvent être observer pendant la dérive.

## Informations complémentaires
 - [Guide SOS](https://saveontarioshipwrecks.ca/diverguides/lock-21/)
 - [Vidéo qui donne une idée des conditions](https://www.youtube.com/watch?v=ZQuZ_Z25xEI)
 - [Article détaillé](https://davidgibbins.com/journal/2016/4/8/diving-lock-21-a-submerged-victorian-canal-lock-on-the-st-lawrence-river-canada)