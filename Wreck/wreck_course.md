# Épaves

## Théorie
Pourquoi plonger en épave ?

### Risques
 - Orientation (pas toujours dans le bon sens, compas attiré par le métal )
 - Visibilité (manque de lumière, différent type de sédiments)
 - Metal (risque de coupure, d'effritement)
 - Effondrement
 - Profondeur
 - Traffic maritime
 - Courants
 
### Respecter l'histoire
Législation canadienne : [Receveur d'épaves](https://tc.canada.ca/fr/marine/receveur-epave-vue-ensemble).
Plus spécialement la [Loi sur les épaves et les bâtiments abandonnés ou dangereux](https://laws-lois.justice.gc.ca/fra/lois/W-12.3/index.html)

### Préparation
 - Choix du site
  - Objectif(s)
  - Limites
  - Points particuliers (courant, température, faune, visibilité)
  - Collecte d'informations
  - Internet (Groupe en ligne (FaceBook, Scubaboard, Instagram...), Vidéos, Photogrametrie, modèle 3d https://dive3d.eu/thistlegorm-cargo/)
  - Guides papiers
  - **Guide / Communauté locale**
 
### Matériel
 - Ligne (reel vs spools, type de ligne)
 - Lampes (types, primaire vs secondaire, emplacement)
 - Air (redondance, long hose, calcul des tiers)
 - Suivant le site (SMB, spool, bouée de surface, marqueur lumineux)
 
### Travailler en Équipe
 - Roles (Leader, ligne, orientation, parachute...)
 - Positions
 - Communications
 - [Briefing](https://www.tdisdi.com/tdi-diver-news/tech-dive-team-briefing/)
 
### En plongée
 - Ligne (placements, suivi 0 visibilité)
 - Positionnement (par rapport à l'équipe, à l'épave)
 - Palmage (frog kick, modified flutter, helicopter turn, back kick)

### Formations Additionnelles
 - [Advanced Wreck TDI](https://www.tdisdi.com/tdi/get-certified/Advanced-Wreck-Diver/)
 - [Cave](https://www.tdisdi.com/tdi/get-certified/Cavern-Diver/)
 - [NAS](https://saveontarioshipwrecks.ca/nas-training/)
 - [Recherche et Récupération](https://www.padi.com/fr/cours/search-and-recovery-diver)

### Informations additionnelles
Plongée d'épave vue par les francais : https://www.plongee-infos.com/plongee-sur-epaves-la-technique-vue-par-les-tekkies

Photogrametrie d'épaves des grands lacs et du St Laurent : https://3dshipwrecks.org/our-models/

Guide des sites de plongée du Québec de Louis Giguère (disponible en bibliothèque)

[Videos](https://www.sidemounting.com/course/free-videos/) de Steve Martin

[Scubatech Philippines](https://scubatechphilippines.com/scuba_blog/advanced_wreck_diving_techniques/) Blog de Andy Davis

[Section épave de Scubarama](https://scubarama.ca/index.php/Cat%C3%A9gorie:%C3%89pave) (Voir la section Ontario pour une carte avec plein d'épaves)

[Accident dans une épave à 120ft (anglais)](https://wreckedinmyrevo.com/2023/11/16/close-call-on-the-ijn-sata-palau-120-fsw/)

## Pratique

### Surface
 - Communications (signe, lampe active/passive)
 - Pose de ligne
 - Suivi de ligne
 
### Piscine
   <details>
     <summary>Team check = START</summary>

  - S-drill (Partage d'air)
  - Team check(Vérification de l'équipement/des bulles)
  - Air (quantité et nature des gas)
  - Route (Points d'entré/sortie, objectif, options)
  - Tables (Limites: profondeur, durée, air, deco au besoin).
  </details>

   <details>
     <summary>Flottabilité et trim</summary>
 - Ludion à plat
 - Changement de masque
 - Manipulation de l'équipement
  </details>
   <details>
     <summary>Palmage</summary>
  - Grenouille
  - Hélicoptere
  - Battements modifiés
  - Arrière
  </details>
   <details>
     <summary>Manipulation du reel/spool</summary>

  - Attache primaire/secondaire
  - Tie-offs
  - Connexion gentleman
  </details>
   <details>
     <summary>Suivi de ligne</summary>
 - Tenue de la ligne
 - Positionnement des plongeurs
 - Avancement
  </details>
   <details>
     <summary>Communications</summary>
 - Lampes actives/passives
 - Communication en contact (0 Vis)
  </details>
 
### Milieu Naturel
#### Jour 1
 - Plongée 1 Rothesay (1867-1889 Long bateau à aubes, coula suite à une collision, détruit par un exercice d'explosif du collège Royal Militaire de Kingston en 1901)
   -  Histoire
   -  Risques
   -  Points d'intérêtes
   -  Profil de plongée

 - Plongée 2 Wee-Hawk (ancienne barge à fleur d'eau avec petite pénetration possible) Conestoga
   - Histoire
   - Risques
   - Points d'intérêtes
   - Profil de plongée

#### Jour 2
A définir (Gaskin ? Power House, Wolf Islander)

### Jour 3
Au choix pour le plaisir (Eastcliffe Hall, US ?)